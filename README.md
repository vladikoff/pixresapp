# Pixr.es

## Step 1: Install your custom Pixr.es app
* Install Meteor
    * ```$ curl https://install.meteor.com | /bin/sh```
* Clone this repository
    * From the project directory, ``cd app``.
    * Run ```meteor deploy [your-app-name-here] --password``` (```--password``` will help you set a password for your app-name)
    * After deployment is complete, proceed to the next step.

## Step 2: Configure

* You've deployed to ```[your-app-name-here]``` in the previous step.
* You must configure twitter auth, go to ```[app_url]/_configure``` in your browser, follow instructions.
