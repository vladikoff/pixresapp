// Shared Code
var Images = new Meteor.Collection("images");

Images.allow({
    insert: function (userId) {
        return Meteor.user() && Meteor.user().profile.approved == true;
    },
    update: function (userId) {
        return false
    },
    remove: function (userId) {
        return Meteor.user() && Meteor.user().profile.approved == true;
    }
});

Meteor.users.allow({
    update: function (loggedInUser, selectedUser) {
        var toUpdate = Meteor.users.findOne({_id: selectedUser[0]._id});
        return Meteor.user() && Meteor.user().profile.approved == true && toUpdate.profile.admin == false;
    },
    remove: function (userId, selectedUser) {
        var toDelete = Meteor.users.findOne({_id: selectedUser[0]._id});
        return Meteor.user() && Meteor.user().profile.approved == true && toDelete.profile.admin == false;
    }
});
