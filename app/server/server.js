/**
 * Publish images by id
 */
Meteor.publish('images', function (list_id) {
    return Images.find({name: list_id});
});


/**
 * Publish history for the logged in user
 */
Meteor.publish('history', function () {
    var thisUser = Meteor.users.findOne({_id: this.userId});
    if (thisUser && thisUser.profile.approved) {
        return Images.find({userId: this.userId}, {sort: {createdAt: -1}, limit: 10});
    }
    return null;
});


/**
 * Publish approved users if logged in
 */
Meteor.publish('approvedUsers', function () {
    var thisUser = Meteor.users.findOne({_id: this.userId});
    if (thisUser && thisUser.profile.approved) {
        return Meteor.users.find({'profile.approved': true});
    }
    return null;
});


/**
 * Publish requested users if logged in
 */
Meteor.publish('requestedUsers', function () {
    var thisUser = Meteor.users.findOne({_id: this.userId});
    if (thisUser && thisUser.profile.approved) {
        return Meteor.users.find({'profile.approved': false});
    }
    return null;
});


/**
 * User Creation, set the first user as admin
 */
Accounts.onCreateUser(function(options, user) {
    if (options.profile) {
        user.profile = {};
        user.profile.options = options.profile;
        // IF USERS COUNT == 0, then set APPROVED USER by default.
        if (Meteor.users.findOne()) {
          user.profile.approved = false;
          user.profile.admin = false;
        } else {
          user.profile.approved = true;
          user.profile.admin = true;
        }
          user.profile.screenName = user.services.twitter.screenName;
    }
    return user;
});

Meteor.startup(function () {

    var closePopup = function(res, query) {
        res.writeHead(200, {'Content-Type': 'text/html'});
        var content =
            //'<html><head><script>window.close()</script></head></html>';
            '<html><head><script>window.location.href="/?authState=' + query.state + '"</script></head></html>';
        res.end(content, 'utf-8');
    };

    // code to run on server at startup
    Accounts.oauth._renderOauthResults = function(res, query) {
        // We support ?close and ?redirect=URL. Any other query should
        // just serve a blank page
        if ('close' in query) { // check with 'in' because we don't set a value
            closePopup(res, query);
        } else if (query.redirect) {
            res.writeHead(302, {'Location': query.redirect});
            res.end();
        } else {
            res.writeHead(200, {'Content-Type': 'text/html'});
            res.end('', 'utf-8');
        }
    };
});