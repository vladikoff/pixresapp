/**
 * Check if we are inside of a frame
 */
Template.footer.frame = top !== self;


/**
 * Footer Events
 */
Template.footer.events({
    // logout
    'click .btn-logout': function (event, template) {
        Meteor.logout(function() {
          if (!window.locationbar.visible) window.close();
          app.navigate("/", {trigger: true});
        });
    },
    // view history
    'click .btn-history': function (event, template) {
        app.navigate("/history", {trigger: true});
    },
    // view users
    'click .btn-users': function (event, template) {
        app.navigate("/users", {trigger: true});
    },
    // view home
    'click .btn-home': function (event, template) {
      app.navigate("/", {trigger: true});
    }
});
