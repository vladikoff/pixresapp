/**
 * if uploading
 */
Template.progress.uploading = function () {
    return Session.equals("uploading", true);
};
