/**
 * Share Events
 */
Template.share.events({
    // share facebook
    'click #btn-facebook': function (event, template) {
       var url = Session.get("imageUrl");
       window.open('http://www.facebook.com/share.php?u=' + url,'sharer','toolbar=0,status=0,width=548,height=325');
    },
    // share twitter
    'click #btn-twitter': function (event, template) {
        var url = Session.get("imageUrl");
        window.open('http://twitter.com/share?url=' + url, 'twitterwindow', 'height=450, width=550, top='+($(window).height()/2 - 225) +', left='+$(window).width()/2 +', toolbar=0, location=0, menubar=0, directories=0, scrollbars=0');
    },
    // share google plus
    'click #btn-gplus': function (event, template) {
        var url = Session.get("imageUrl");
        window.open('https://plus.google.com/share?url='+url, 'popupwindow', 'scrollbars=yes,width=800,height=400');
    }
});
