/**
 * Image path
 * @return {Boolean}
 */
Template.image.img = function () {
    var docImg = Images.findOne({name: Session.get("i")});
    Session.set("imageUrl", "http://" + CONFIG.SITE_URL + "/i/" + Session.get("i"));
    return docImg ? docImg.image : "";
};


/**
 * get image
 */
Template.image.image = function () {
    if (top === self) {
      return Session.equals("page", "image") && Session.get("i");
    } else {
      window.open("http://" + CONFIG.SITE_URL + "/i/" + Session.get("i"),'_blank','width=800, height=900');
      return false;
    }
};


/**
 * Image URL
 * @return {String}
 */
Template.image.img_url = function () {
    return "http://" + CONFIG.SITE_URL + "/i/" + Session.get("i");
};
