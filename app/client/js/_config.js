/**
 * App configuration variables
 * @type {Object}
 */
var CONFIG = {
    SITE_URL: location.host,
    VERSION: "1.0"
};
