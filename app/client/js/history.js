/**
 * Set history app state
 */
Template.history.history = function() {
    return Session.equals("page","history");
};


/**
 * set history images
 */
Template.history.images = function() {
    var images = Images.find({}, {sort: {createdAt: -1}});
    return images.fetch();
};


/**
 * check if inside of a frame
 */
Template.history.frame = top !== self;


/**
 * current user
 */
Template.history.user = function() {
  return Meteor.user();
};


/**
 * site url
 */
Template.history.url = function () {
  return CONFIG.SITE_URL === "pixr.es" || CONFIG.SITE_URL === "www.pixr.es" ? "" : CONFIG.SITE_URL;
};


/**
 * rendered animation
 */
Template.history.rendered = function() {
    if(top === self) {
      if (this.findAll("img")) {
          $(this.findAll("img")).addClass('animated bounce');
      }
    }
};


/**
 * History Events
 */
Template.history.events({
    'click img': function (event, template) {
        Session.set("i", this.name);
        app.navigate("/i/"+ this.name, {trigger: true});
    },
    'click .loginFrame': function (event, template) {
      window.open("/",'login','toolbar=0,status=0,width=600,height=400');
    }
});
