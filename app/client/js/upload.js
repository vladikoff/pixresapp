/**
 * if upload state
 */
Template.upload.state = function () {
    return Session.equals("uploading", false) && (Session.equals("page","upload") || Session.equals("page","image"));
};


/**
 * if page is image
 */
Template.upload.image = function () {
    return Session.equals("page","image");
};


/**
 * if user is pending
 */
Template.upload.pending = function () {
  return (! Meteor.loggingIn() && Meteor.user() && Meteor.user().profile && ! Meteor.user().profile.approved);
};


/**
 * if user and user profile
 */
Template.upload.user = function() {
  return Meteor.user() && Meteor.user().profile;
};


/**
 * if user approved
 */
Template.upload.approved = function() {
    return Meteor.user().profile && Meteor.user().profile.approved;
};

/**
 * Upload Events
 */
Template.upload.events({
    // take photo
    'click #take': function (event, template) {
        $("#choose").click();
    },
    // choose file
    'change #choose': function (event, template) {
        Session.set("uploading", true);
        Session.set("i", null);

        var file = event.target.files[0];

        if (file) {
            var reader = new FileReader();
            reader.onload = function (e) {
                var img = new Image();

                img.onload = function(){
                    var orientation = (exif && exif['Orientation']) ? exif['Orientation'] : '';
                    var name = "";
                    var possible = "ABCDEFGHJKMNOPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz123456789-_";

                    for( var i=0; i < 11; i++ )
                        name += possible.charAt(Math.floor(Math.random() * possible.length));

                    var canvas = document.createElement('canvas');

                    var imgWidth = img.width, imgHeight = img.height,
                        maxWidth = 600, maxHeight = 700,
                        width = imgWidth,
                        height = imgHeight;

                    if (maxWidth && width > maxWidth) {
                        width = maxWidth;
                        height = Math.floor(imgHeight * width / imgWidth);
                    }
                    if (maxHeight && height > maxHeight) {
                        height = maxHeight;
                        width = Math.floor(imgWidth * height / imgHeight);
                    }
                    var opt = { width : width, height : height, orientation: orientation };

                    renderImageToCanvas(img, canvas, opt);
                    var imageData = canvas.toDataURL("image/jpeg", 0.5);

                    Images.insert(
                        {
                          image: imageData,
                          name: name,
                          userId: Meteor.userId(),
                          createdAt: new Date().getTime()
                        },

                        function(error, result) {
                          Session.set("uploading", false);
                          app.navigate("i/" + name, {trigger: true});
                        }
                    );
                };
                img.src = e.target.result;

                var byteString = atob(img.src.split(',')[1]);
                var binary = new BinaryFile(byteString, 0, byteString.length);
                var exif = EXIF.readFromBinaryFile(binary);


            };
            reader.readAsDataURL(file);

        }
    },
    // login to twitter
    'click .l-twitter': function () {
        // get on the iOS train
        var iOS = ( navigator.userAgent.match(/(iPad|iPhone|iPod)/i) ? true : false );
        if (iOS) {
        //if (true) {
            myLoginWithTwitter(function(err) { });
        } else {
            Meteor.loginWithTwitter();
        }
    }
});

var myLoginWithTwitter = function (options, callback) {

    // support both (options, callback) and (callback).
    if (!callback && typeof options === 'function') {
        callback = options;
        options = {};
    }

    var config = Accounts.loginServiceConfiguration.findOne({service: 'twitter'});
    if (!config) {
        callback && callback(new Accounts.ConfigError("Service not configured"));
        return;
    }

    var state = Meteor.uuid();
    var callbackUrl = Meteor.absoluteUrl('_oauth/twitter?close&state=' + state);

    // url to app, enters "step 1" as described in
    // packages/accounts-oauth1-helper/oauth1_server.js
    var url = '/_oauth/twitter/?requestTokenAndRedirect=' + encodeURIComponent(callbackUrl) +
        '&state=' + state;

    myInitiateLogin(state, url, callback);
};

var myInitiateLogin = function (state, url, callback, dimensions) {

    // don't bother with meteor's popup stuff, commenting
    // XXX these dimensions worked well for facebook and google, but
    // it's sort of weird to have these here. Maybe an optional
    // argument instead?
    //var popup = openCenteredPopup(
    //  url,
    //  (dimensions && dimensions.width) || 650,
    //  (dimensions && dimensions.height) || 331);

    // at this point, instead of the popup and waiting to close approach,
    // we open the twitter auth dialog "in place"
    var popup = window.open(url, '_self');

    // as noted above, we are going a diff path so this does not matter
    //if (popup.focus) {
    //  //popup.focus();
    //  console.log("new window has focus");
    //}
    //var checkPopupOpen = setInterval(function() {
    //  console.log("checking for auth result...");
    //  // Fix for #328 - added a second test criteria (popup.closed === undefined)
    //  // to humour this Android quirk:
    //  // http://code.google.com/p/android/issues/detail?id=21061
    //  if (popup.closed || popup.closed === undefined) {
    //    clearInterval(checkPopupOpen);
    //    tryLoginAfterPopupClosed(state, callback);
    //  }
    //}, 100);
};
