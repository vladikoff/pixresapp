/**
 * app url
 */
Template.title.url = function () {
    return (/pixr.es/).test(CONFIG.SITE_URL) ? false : CONFIG.SITE_URL;
};


/**
 * custom title thing
 * @return {*}
 */
Template.title.thing = function () {
    var things = ["accessory", "accomplice", "affiliate", "aid", " ally", " assistant", "auxiliary", "branch",
        "buddy", "bro", "clubber", "co-worker", "cohort", "collaborator", "companion", "compatriot",
        "comrade", "confederate", "consort", "cooperator", "crony", "fellow", "friend", "helper",
        "joiner", "kissing cousin", "mate", "offshoot", "pal", "pard",
        "partner", "peer", "playmate", "sidekick", "teammate"];
    return things[Math.floor(Math.random() * things.length)];
};
