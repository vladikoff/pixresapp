/**
 * if page state is users
 */
Template.users.users = function() {
    return Session.equals("page","users");
};


/**
 * get approved used
 */
Template.users.approvedUsers = function() {
    return Meteor.users.find({'profile.approved': true});
};


/**
 * get pending users
 */
Template.users.requestedUsers = function() {
    return Meteor.users.find({'profile.approved': false});
};


/**
 * User Events
 */
Template.users.events({
    // open links in a new window
    'click a': function (e, template) {
        window.open(e.target.href, '_blank');
        return false;
    },
    // approve a user
    'click .approve': function (e, template) {
        Meteor.users.update(this._id,{ $set: { 'profile.approved': true }});
    },
    // delete user
    'click .delete': function (e, template) {
        var self = this;

        Meteor.users.remove(this._id, function() {
            if (self._id == Meteor.userId()) {
                app.navigate("/", {trigger: true});
            }
        });
    }
});
