/**
 * Custom iOS Route until Auth is fixed
 * @param state
 * @param callback
 */
// NOTE: this is getting called by the simple route handler above
// if we get back a state value then we try to login using this
//
// Send an OAuth login method to the server. If the user authorized
// access in the popup this should log the user in, otherwise
// nothing should happen.
function tryLoginAfterPopupClosed (state, callback) {
    Meteor.apply('login', [
        {oauth: {state: state}}
    ], {wait: true}, function(error, result) {
        if (error) {
            // got an error from the server. report it back.
            callback && callback(error);
        } else if (!result) {
            // got an empty response from the server. This means our oauth
            // state wasn't recognized, which could be either because the
            // popup was closed by the user before completion, or some sort
            // of error where the oauth provider didn't talk to our server
            // correctly and closed the popup somehow.
            //
            // we assume it was user canceled, and report it as such. this
            // will mask failures where things are misconfigured such that
            // the server doesn't see the request but does close the
            // window. This seems unlikely.
            callback &&
            callback(new Accounts.LoginCancelledError("Popup closed"));
        } else {
            Accounts._makeClientLoggedIn(result.id, result.token);
            callback && callback();
        }
    });
}


/**
 * Temporary Match iOS Custom Auth Route
 * @type {RegExp}
 */
var patt=/(authState=)(.*)/;
match = document.URL.match(patt);
if (match) {
    var state = match[2];
    this.tryLoginAfterPopupClosed(state, function(err, res) {
        if (err) { return; }
    });
}


/**
 * Backbone router to control the states of this app
 */
var Router = Backbone.Router.extend({
    routes: {
        "":            "upload",
        "i/:query":    "image",
        "history":     "history",
        "users":       "users",
        "login":       "login",
        "_configure":  "_configure",
        "*path":           "upload"
    },

    /**
     * Upload state
     */
    upload: function() {
        Session.set("page", "upload");
        Session.set("uploading", false);
        if (window.name === "PixrEsFrame") app.navigate("/history", {trigger: true});
    },


    /**
     * View Image state
     * @param image , name url
     */
    image: function(image) {
        Session.set("page", "image");
        Session.set("i", image);
    },


    /**
     * View Users
     */
    users: function() {
        Session.set("page", "users");
        checkLoggedIn();
        checkApproved();
    },


    /**
     * View History
     */
    history: function() {
        Session.set("page", "history");
        //checkLoggedIn();
        //checkApproved();

    },


    /**
     * Custom login for iframe
     */
    login: function() {
      if (top !== self) {
        var state = Meteor.uuid();
        // We need to keep state across the next two "steps" so we"re adding
        // a state parameter to the url and the callback url that we"ll be returned
        // to by oauth provider

        // url back to app, enters "step 2" as described in
        // packages/accounts-oauth1-helper/oauth1_server.js
        var callbackUrl = Meteor.absoluteUrl("_oauth/twitter?close&state=" + state);

        // url to app, enters "step 1" as described in
        // packages/accounts-oauth1-helper/oauth1_server.js
        var url = "/_oauth/twitter/?requestTokenAndRedirect="
          + encodeURIComponent(callbackUrl)
          + "&state=" + state;
        window.open(url,"login","toolbar=0,status=0,width=548,height=325");
      }
    },

    /**
     * Configure twitter auth on first use
     * @private
     */
    _configure: function() {
        Session.set("page", "configure");
    }
});


// set app router
var app = new Router;


/**
 * Meteor startup!
 */
Meteor.startup(function () {
  // start Backbone history
  Backbone.history.start({pushState: true});
});


/**
 * Subscribe to collections
 */
Meteor.autosubscribe(function () {
    Meteor.subscribe("images", Session.get("i"));
    Meteor.subscribe("history");
    Meteor.subscribe("approvedUsers");
    Meteor.subscribe("requestedUsers");
});


/**
 * Meteor Autorun
 */
Meteor.autorun(function() {
    // no need to configure if we have a user already
    if (Meteor.user() && Session.equals("page", "configure")) app.navigate("/", {trigger: true});

    // close the window if logged in and in a frame
    if ( (window.name == "Login" || window.name == "login") && Session.equals("page","upload") && Meteor.user()) window.close();
});


/**
 * Check if the user is logged in, redirect
 */
function checkLoggedIn() {
    if(! Meteor.user()) app.navigate("/", {trigger: true});
}


/**
 * Check if the user is approved, redirect
 */
function checkApproved() {
    if(Meteor.user().profile && ! Meteor.user().profile.approved) app.navigate("/", {trigger: true});
}
